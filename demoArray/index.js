// pass by value : number,string,boolean,..

// pass by reference : array, object,..

// before  40sdt => 40 variable

// after 40sdt => 1 variable

//  thường được dùng để lưu dữ liệu cùng loại

var numberArr = ["alice", "bob", "Mina", 23];
// console.log(numberArr[2]);
for (var index = 0; index < numberArr.length; index++) {
  var current = numberArr[index];
  // console.log("current", current);
}
// console.log(numberArr[1]);
// phần tử
// truy cập tới phần tử thì phải có vị trí

// vị trí = index . index luôn luôn bắt đầu từ 0

numberArr[0] = "Alice Nguyễn";

// console.log("numberArr", numberArr);

// số lượng phần tử

var totalElement = numberArr.length;

// console.log({ totalElement });

// forEach
// console.log("numberArr 34", numberArr);
// unmutable
numberArr.forEach(function (item, index) {
  // console.log(item);
  // numberArr[index] = "Bob";
  item = 2;
});

// console.log("numberArr 42", numberArr);
//  kết thúc tại đây

// high order fuction

// callback =>  khó

function sayHello(title, name) {
  // console.log("say hello to" + "  " + title + "  " + name);
}

// sayHello("Alice");

function introduce(callBack, name) {
  callBack("em", name);
}

introduce(sayHello, "Alice");

//  push thêm 1 phần tử vào cuối array

var menu = ["Búm bò", "Bún riu"];

// mutable : thay đổi array gốc
// console.log("Menu", menu);
menu.push("Cơm tấm");
// console.log("Menu", menu);

var monAn = menu.pop();

// console.log("monAn", monAn);
// console.log("Menu", menu);

var colors = ["red", "black", "green", "blue", "green"];
//  chỉ trả về vị trí đầu tiên khớp
var indexGreen = colors.indexOf("green");

console.log("indexGreen", indexGreen);

if (indexGreen !== -1) {
  console.log("có có green");
} else {
  console.log("không màu green");
}

// slice : vị trí start,  end  . Unmutable
// splice: vị trí start, số lượng.  mutable ( thay đổi array gốc )

const months = ["Jan", "March", "April", "June"];
months.splice(1, 1);
console.log(months);

var monAn = ["Bún riu", "Bún bò", "Bún thái"];
var indexBunBo = monAn.indexOf("Bún bò");
if (indexBunBo !== -1) {
  monAn.splice(indexBunBo, 1);
}

console.log("monAn đã xoá bún bò", monAn);
