//  function trả về tên user từ 1 thẻ tr bất kì
function getNameFromTrTag(trTag) {
  var tdList = trTag.querySelectorAll("td");

  return tdList[2].innerText;
}
function getScoreFromTrTag(trTag) {
  var tdList = trTag.querySelectorAll("td");
  return tdList[3].innerText * 1;
}

var tdList = document.querySelectorAll(".td-scores");

console.log(tdList[4].innerText);
// array chứa điểm số
let scoreArr = [];

for (var index = 0; index < tdList.length; index++) {
  //  thẻ Td trong mỗi lần lặp
  var currentTd = tdList[index];
  // điểm số của thẻ td hiện tại
  var number = currentTd.innerText * 1;

  scoreArr.push(number);
}
// đi tìm số lớn nhất
var max = scoreArr[0];

for (index = 1; index < scoreArr.length; index++) {
  if (scoreArr[index] > max) {
    max = scoreArr[index];
  }
}

console.log("max", max);

// var nums = [200, 4, 8, 1, 6, 100];

// var max = nums[0];

// for (index = 0; index < nums.length; index++) {
//   if (nums[index] > max) {
//     max = nums[index];
//   }
// }

// console.log("max", max);

//  danh sách thẻ tr nằm trong tbody

var trList = document.querySelectorAll("#tblBody tr");

// console.log("trList", trList);

var trMax = trList[0];
// var max = scoreArr[0];
// for (index = 1; index < scoreArr.length; index++) {
//   if (scoreArr[index] > max) {
//     max = scoreArr[index];
//   }
// }
for (var index = 0; index < trList.length; index++) {
  var currentTr = trList[index];
  //   chứa điểm số của thẻ tr trong mỗi lần duyệt
  var currentScoreTr = getScoreFromTrTag(currentTr);
  var currentScoreMax = getScoreFromTrTag(trMax);
  if (currentScoreTr > currentScoreMax) {
    trMax = currentTr;
  }
}

var username = getNameFromTrTag(trMax);
var score = getScoreFromTrTag(trMax);

document.getElementById("svGioiNhat").innerText = ` ${username} -  ${score}`;

console.log(score, username);

// console.log("trMax", trMax);

// danh sách thẻ td nằm trong thẻ tr có tên trMax
var list = trMax.querySelectorAll("td");

// in ra màn hình tên user nằm trong thẻ tr có tên trMax
console.log(list[3].innerText);
